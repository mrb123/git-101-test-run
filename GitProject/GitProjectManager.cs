﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GitProject.Classes;

namespace GitProject
{
    public class GitProjectManager
    {
        private readonly IMessage _mainClass;
        private readonly IMessage _secondClass;
        private readonly IMessage _thirdClass;
        private readonly IMessage _fifthClass;

        public GitProjectManager(IMessage mainClass, 
                                IMessage secondClass,
                                IMessage thirdClass,
                                IMessage fifthClass)
        {
            _mainClass = mainClass;
            _secondClass = secondClass;
            _thirdClass = thirdClass;
            _fifthClass = fifthClass;
        }

        public void ShowMessages()
        {
            Console.WriteLine(_mainClass.GetMessage());
            Console.WriteLine(_secondClass.GetMessage());
            Console.WriteLine(_thirdClass.GetMessage());
            Console.WriteLine(_fifthClass.GetMessage());

            Console.ReadLine();
        }
    }
}
